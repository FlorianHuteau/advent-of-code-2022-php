<?php
$filename = "input";
$file = fopen($filename, 'rb');

// Array of alphabet
$alphabet = range('a', 'z');
$x = $y = 0;
$map = array();
$start = $end = [0, 0];
if ($file) {
    while (($line = fgets($file)) !== false) {
        $line = str_split(trim($line));
        $convert = array();
        foreach ($line as $char) {
            if ($char === 'S') {
                $start = [$y, $x];
                $convert[] = 0;
            } elseif ($char === 'E') {
                $end = [$y, $x];
                $convert[] = 100;
            } else {
                $convert[] = array_search($char, $alphabet, true);
            }
            $x++;
        }
        $y++;
        $x = 0;
        $map[] = $convert;
    }
    fclose($file);
} else {
    echo "Error opening file";
}

function findPathTo100($matrix): string
{
    $rows = count($matrix);
    $cols = count($matrix[0]);

    $startRow = 0;
    $startCol = 0;
    $targetValue = 100;

    // Trouver la position de départ
    foreach ($matrix as $r => $rValue) {
        for ($c = 0; $c < $cols; $c++) {
            if ($rValue[$c] === 1) {
                $startRow = $r;
                $startCol = $c;
                break 2; // Sortir des deux boucles
            }
        }
    }

    // Fonction de calcul de la distance heuristique (estimation de la distance au point final)
    function heuristic($r, $c) : float|int
    {
        return abs($r + $c - 100);
    }

    // Noeud de l'algorithme A*
    class Node {
        public $row;
        public $col;
        public $value;
        public $g; // Coût depuis le point de départ
        public $h; // Estimation du coût jusqu'à l'objectif
        public $f; // Somme de g et h

        public function __construct($row, $col, $value, $g, $h) {
            $this->row = $row;
            $this->col = $col;
            $this->value = $value;
            $this->g = $g;
            $this->h = $h;
            $this->f = $g + $h;
        }
    }

    // Appeler l'algorithme A*
    $openSet = [new Node($startRow, $startCol, $matrix[$startRow][$startCol], 0, heuristic($startRow, $startCol))];
    $closedSet = [];

    while (!empty($openSet)) {
        // Trouver le nœud avec la plus petite valeur f dans openSet
        $currentNode = array_shift($openSet);

        if ($currentNode->value === $targetValue) {
            return "Chemin trouvé jusqu'à 100 avec un minimum de {$currentNode->g} déplacements !";
        }

        $closedSet[] = $currentNode;

        // Explorer les voisins
        for ($dr = -1; $dr <= 1; $dr++) {
            for ($dc = -1; $dc <= 1; $dc++) {
                if (($dr !== 0 || $dc !== 0) && abs($dr) + abs($dc) === 1) {
                    $newRow = $currentNode->row + $dr;
                    $newCol = $currentNode->col + $dc;

                    if ($newRow >= 0 && $newRow < $rows && $newCol >= 0 && $newCol < $cols) {
                        $newValue = $matrix[$newRow][$newCol];
                        if ($newValue >= $currentNode->value - 1 && $newValue <= $currentNode->value + 1) {
                            $newG = $currentNode->g + 1;
                            $newH = heuristic($newRow, $newCol);
                            $newNode = new Node($newRow, $newCol, $newValue, $newG, $newH);

                            $skip = false;
                            foreach ($closedSet as $closedNode) {
                                if ($closedNode->row === $newNode->row && $closedNode->col === $newNode->col && $closedNode->f <= $newNode->f) {
                                    $skip = true;
                                    break;
                                }
                            }

                            if (!$skip) {
                                foreach ($openSet as $openNode) {
                                    if ($openNode->row === $newNode->row && $openNode->col === $newNode->col && $openNode->f <= $newNode->f) {
                                        $skip = true;
                                        break;
                                    }
                                }
                            }

                            if (!$skip) {
                                $openSet[] = $newNode;
                            }
                        }
                    }
                }
            }
        }

        usort($openSet, function($a, $b) {
            return $a->f - $b->f;
        });
    }

    return "Aucun chemin trouvé jusqu'à 100.";
}

// Exemple de matrice (tableau 2D)
$matrix = array(
    array(1, 2, 2),
    array(3, 3, 8),
    array(5, 4, 100)
);

$result = findPathTo100($matrix);
echo $result;
