<?php
$filename = "input";
$file = fopen($filename, 'rb');


$total = 0;
if ($file) {
    while (($line = fgets($file)) !== false && $line !== "\r\n") {
        $line = explode(',', $line);
        $elf1 = explode('-', $line[0]);
        $elf2 = explode('-', $line[1]);

        $list1 = array();
        $list2 = array();
        for ($i = (int)$elf1[0]; $i <= (int)$elf1[1]; $i++) {
            $list1[] = $i;
        }
        for ($i = (int)$elf2[0]; $i <= (int)$elf2[1]; $i++) {
            $list2[] = $i;
        }

        $x = 0;
        $y = 0;
//        foreach ($list1 as $value) {
//            if (in_array($value, $list2, true)) {
//                $x++;
//            }
//        }
        foreach ($list2 as $value) {
            if (in_array($value, $list1, true)) {
                $y++;
            }
        }
        if ($y > 0) {

            $total++;
        }
    }
    fclose($file);
} else {
    echo "Error opening file";
}

echo $total;