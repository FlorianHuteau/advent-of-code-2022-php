# Utilisez l'image de base PHP 8.2-cli
FROM php:8.2-cli

# Installez l'extension BCMath
RUN docker-php-ext-install bcmath
