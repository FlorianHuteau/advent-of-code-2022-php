<?php
$filename = "input";
$file = fopen($filename, 'rb');

$cycle = 1;
$x = 1;
$total = 0;
$ctr = array_fill(0, 6, array_fill(0, 40, '.'));
if ($file) {
    while (($line = fgets($file)) !== false) {
        if(trim($line) !== "noop") {
            $line = explode(' ', trim($line));
            addCycle();
            $x += (int)$line[1];
        }
        addCycle();

    }
    fclose($file);
} else {
    echo "Error opening file";
}

echo $total . "\n";
foreach ($ctr as $row) {
    echo implode('', $row) . "\n";
}

function addCycle(): void
{
    global $cycle, $x, $total, $ctr;
    $cycle++;
    if($cycle === 20 || ($cycle - 20) % 40 === 0) {
        $total += $cycle * $x;
    }
    $position = $cycle - 40 * floor($cycle/40);
    if($position >= $x && $position <= $x + 2) {
        $ctr[floor($cycle/40)][$position - 1] = '#';
    }
}