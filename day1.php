<?php
$filename = "input";
$file = fopen($filename, 'rb');

$numbers = array();
$i = 0;
if ($file) {
    while (($line = fgets($file)) !== false) {
        if ($line === "\r\n") {
            $i++;
            $numbers[$i] = 0;
        } else {
            $numbers[$i] += (int)$line;
        }
    }
    fclose($file);
} else {
    echo "Error opening file";
}

rsort($numbers);

echo $numbers[0] + $numbers[1] + $numbers[2];

