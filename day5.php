<?php
$filename = "input";
$file = fopen($filename, 'rb');

//[N] [G]                     [Q]
//[H] [B]         [B] [R]     [H]
//[S] [N]     [Q] [M] [T]     [Z]
//[J] [T]     [R] [V] [H]     [R] [S]
//[F] [Q]     [W] [T] [V] [J] [V] [M]
//[W] [P] [V] [S] [F] [B] [Q] [J] [H]
//[T] [R] [Q] [B] [D] [D] [B] [N] [N]
//[D] [H] [L] [N] [N] [M] [D] [D] [B]
// 1   2   3   4   5   6   7   8   9
$stack = [
    ['D', 'T', 'W', 'F', 'J', 'S', 'H', 'N'],
    array_reverse(['G', 'B', 'N', 'T', 'Q', 'P', 'R', 'H']),
    array_reverse(array('V', 'Q', 'L')),
    array_reverse(array('Q', 'R', 'W', 'S', 'B', 'N')),
    array_reverse(array('B', 'M', 'V', 'T', 'F', 'D', 'N')),
    array_reverse(array('R', 'T', 'H', 'V', 'B', 'D', 'M')),
    array_reverse(array('J', 'Q', 'B', 'D')),
    array_reverse(array('Q', 'H', 'Z', 'R', 'V', 'J', 'N', 'D')),
    array_reverse(array('S', 'M', 'H', 'N', 'B')),
];


if ($file) {
    while (($line = fgets($file)) !== false) {
        $line = explode(' ', $line);
        $nCrates = (int)$line[1];
        $pos1 = (int)$line[3];
        $pos2 = (int)$line[5];

        $crates = array();
        for($i = 0; $i < $nCrates; $i++) {
             $crates[] = array_pop($stack[$pos1 - 1]);
        }
        foreach (array_reverse($crates) as $key => $value) {
            $stack[$pos2 - 1][] = $value;
        }
    }
    fclose($file);
} else {
    echo "Error opening file";
}

foreach ($stack as $key => $value) {
    echo array_pop($value);
}