<?php
$filename = "input";
$file = fopen($filename, 'rb');

$roundMax = 10000;
$monkeys = [];
$modulo = 1;
if ($file) {
    while (($line = fgets($file)) !== false) {
        $line = explode(" ", trim($line));
        switch ($line[0]) {
            case "Monkey":
                echo "Création d'un singe \n";
                $monkey = new Monkey((int)$line[1]);
                $monkeys[] = $monkey;
                break;
            case "Starting":
                echo "Ajout d'item \n";
                $monkey->startingItems($line);
                break;
            case "Operation:":
                echo "Ajout d'opération \n";
                $size = count($line);
                $monkey->operation = [$line[$size - 2], $line[$size - 1]];
                break;
            case "Test:":
                echo "Ajout de test \n";
                $monkey->test = end($line);
                $modulo *= (int)end($line);
                break;
            case "If":
                if ($line[1] === "true:") {
                    echo "Ajout de true \n";
                    $monkey->ifTrue = (int)end($line);
                    echo $monkey->ifTrue . "\n";
                } else {
                    echo "Ajout de false \n";
                    $monkey->ifFalse = (int)end($line);
                    echo $monkey->ifFalse . "\n";
                }
                break;
        }
    }
    echo "Monkey trié \n";
    foreach ($monkeys as $monkey) {
        echo "Singe " . $monkey->id . " : " . $monkey->itemToString() . "\n";
    }
    for ($round = 0; $round < $roundMax; $round++) {
        foreach ($monkeys as $monkey) {
            while (count($monkey->items) > 0) {
                $monkey->doInspection($modulo);
                $monkeys[$monkey->doTest()]->items[] = $monkey->getItem();
            }
        }
        echo "round " . $round + 1 . " effectué \n";
//        foreach ($monkeys as $monkey) {
//            echo "Singe " . $monkey->id . " : " . $monkey->itemToString() . "\n";
//        }
    }
    echo "round 20 effectué \n";
    $totalInspection = [];
    foreach ($monkeys as $monkey) {
        echo "Singe " . $monkey->id . " a fait " . $monkey->nbInspection . " inspections \n";
        $totalInspection[] = $monkey->nbInspection;
    }

    rsort($totalInspection);

    echo $totalInspection[0] * $totalInspection[1];
    fclose($file);
} else {
    echo "Error opening file";
}

class Item
{
    public string $worry;

    public function __construct(int $worry)
    {
        $this->worry = (string) $worry;
    }

    public function addWorry(string $inspection): void
    {
        $inspection =  $inspection === "old" ? $this->worry : $inspection;
        $this->worry = bcadd($this->worry, $inspection);
    }

    public function multiplyWorry(string $inspection): void
    {
        $inspection = $inspection === "old" ? $this->worry : $inspection;
        $this->worry = bcmul($this->worry, $inspection);
    }

    public function lessWorry(int $modulo): void
    {
        $this->worry %= $modulo;
    }
}

class Monkey
{
    public int $id;
    public array $items;
    public array $operation;
    public int $nbInspection = 0;
    public string $test;
    public int $ifTrue;
    public int $ifFalse;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function doInspection(int $modulo): void
    {
        $item = $this->items[0];
        switch ($this->operation[0]) {
            case '+':
                $item->addWorry($this->operation[1]);
                break;
            case '*':
                $item->multiplyWorry($this->operation[1]);
                break;
        }
        $item->lessWorry($modulo);
        $this->nbInspection++;
    }

    public function doTest(): int
    {
        $item = $this->items[0];
        $worry =number_format((float)$item->worry, 20, '.', '');
        if (bcmod($worry, $this->test) === "0") {
//            $item->worry = $this->test;
            return $this->ifTrue;
        }

        return $this->ifFalse;
    }

    public function startingItems(array $items): void
    {
        foreach ($items as $item) {
            switch ($item) {
                case "Starting":
                case "items:":
                    break;
                default :
                    $this->items[] = new Item((int)$item);
            }
        }
    }

    public function itemToString(): string
    {
        return implode(', ', array_map(static function ($item) {
            return $item->worry;
        }, $this->items));
    }

    public function getItem(): Item
    {
        return array_shift($this->items);
    }
}