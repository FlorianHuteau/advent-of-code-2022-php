<?php
$filename = "input";
$file = fopen($filename, 'rb');

$shapes = ['P' => 1, 'F' => 2, 'C' => 3];
$points = ['L' => 0, 'W' => 6, 'D' => 3];

$score = 0;

if ($file) {
    while (($line = fgets($file)) !== false) {
        $line = str_split($line);
        if ($line[0] === "A") {
            if ($line[2] === "X") {
                $score += $shapes['C'] + $points['L'];
            } elseif ($line[2] === "Y") {
                $score += $shapes['P'] + $points['D'];
            } elseif ($line[2] === "Z") {
                $score += $shapes['F'] + $points['W'];
            }
        } elseif ($line[0] === "B"){
            if ($line[2] === "X") {
                $score += $shapes['P'] + $points['L'];
            } elseif ($line[2] === "Y") {
                $score += $shapes['F'] + $points['D'];
            } elseif ($line[2] === "Z") {
                $score += $shapes['C'] + $points['W'];
            }
        } elseif ($line[0] === "C"){
            if ($line[2] === "X") {
                $score += $shapes['F'] + $points['L'];
            } elseif ($line[2] === "Y") {
                $score += $shapes['C'] + $points['D'];
            } elseif ($line[2] === "Z") {
                $score += $shapes['P'] + $points['W'];
            }
        }
    }
    fclose($file);
} else {
    echo "Error opening file";
}

echo $score;