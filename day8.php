<?php
$filename = "input";
$file = fopen($filename, 'rb');

$forrest = array();
if ($file) {
    while (($line = fgets($file)) !== false) {
        $trees = str_split(trim($line));
        $forrest[] = $trees;
    }
    fclose($file);
} else {
    echo "Error opening file";
}

// Part 1

$nbCol = count($forrest);
$nbRow = count($forrest[0]);
$totalTrees = $nbRow * $nbCol;
$invisibleTrees = 0;
for ($y = 1; $y < $nbCol - 1; $y++) {
    for ($x = 1; $x < $nbRow - 1; $x++) {
        $tree = (int)$forrest[$y][$x];
        $isInvisibleYm1 = true;
        $isInvisibleYp1 = true;
        $isInvisibleXm1 = true;
        $isInvisibleXp1 = true;

        if ($tree > (int)$forrest[$y - 1][$x]) {
            $isInvisibleYm1 = false;
            for ($i = $y - 1; $i >= 0; $i--) {
                if ($tree <= (int)$forrest[$i][$x]) {
                    $isInvisibleYm1 = true;
                    break;
                }
            }
        }
        if ($tree > (int)$forrest[$y + 1][$x]) {
            $isInvisibleYp1 = false;
            for ($i = $y + 1; $i < $nbCol; $i++) {
                if ($tree <= (int)$forrest[$i][$x]) {
                    $isInvisibleYp1 = true;
                    break;
                }
            }
        }
        if ($tree > (int)$forrest[$y][$x - 1]) {
            $isInvisibleXm1 = false;
            for ($i = $x - 1; $i >= 0; $i--) {
                if ($tree <= (int)$forrest[$y][$i]) {
                    $isInvisibleXm1 = true;
                    break;
                }
            }
        }
        if ($tree > (int)$forrest[$y][$x + 1]) {
            $isInvisibleXp1 = false;
            for ($i = $x + 1; $i < $nbRow; $i++) {
                if ($tree <= (int)$forrest[$y][$i]) {
                    $isInvisibleXp1 = true;
                    break;
                }
            }
        }
        if ($isInvisibleYm1 && $isInvisibleYp1 && $isInvisibleXm1 && $isInvisibleXp1) {
            $invisibleTrees++;
        }
    }
}


echo $totalTrees - $invisibleTrees . "\n";

// Part 2

$max = 0;
for ($y = 0; $y < $nbCol; $y++) {
    for ($x = 0; $x < $nbRow; $x++) {
        $tree = (int)$forrest[$y][$x];
        $nbUp = 0;
        $nbDown = 0;
        $nbLeft = 0;
        $nbRight = 0;

        for ($i = $y - 1; $i >= 0; $i--) {
            if ($tree > (int)$forrest[$i][$x]) {
                $nbUp++;
            } else {
                $nbUp++;
                break;
            }
        }

        for ($i = $y + 1; $i < $nbCol; $i++) {
            if ($tree > (int)$forrest[$i][$x]) {
                $nbDown++;
            } else {
                $nbDown++;
                break;
            }
        }

        for ($i = $x - 1; $i >= 0; $i--) {
            if ($tree > (int)$forrest[$y][$i]) {
                $nbLeft++;
            } else {
                $nbLeft++;
                break;
            }
        }

        for ($i = $x + 1; $i < $nbRow; $i++) {
            if ($tree > (int)$forrest[$y][$i]) {
                $nbRight++;
            } else {
                $nbRight++;
                break;
            }
        }

//        echo $tree.' : '. $nbUp . ' ' . $nbDown . ' ' . $nbLeft . ' ' . $nbRight ." = " . $nbUp * $nbDown * $nbLeft * $nbRight . "\n" ;
        $max = max($nbUp * $nbDown * $nbLeft * $nbRight, $max);
    }
}

echo $max . "\n";