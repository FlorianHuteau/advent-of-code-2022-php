<?php
$filename = "input";
$file = fopen($filename, 'rb');

// array lowercase a = 1 through z = 26
$alphabet = array(
    'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4,
    'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8,
    'i' => 9, 'j' => 10, 'k' => 11, 'l' => 12,
    'm' => 13, 'n' => 14, 'o' => 15, 'p' => 16,
    'q' => 17, 'r' => 18, 's' => 19, 't' => 20,
    'u' => 21, 'v' => 22, 'w' => 23, 'x' => 24,
    'y' => 25, 'z' => 26, 'A' => 27, 'B' => 28,
    'C' => 29, 'D' => 30, 'E' => 31, 'F' => 32,
    'G' => 33, 'H' => 34, 'I' => 35, 'J' => 36,
    'K' => 37, 'L' => 38, 'M' => 39, 'N' => 40,
    'O' => 41, 'P' => 42, 'Q' => 43, 'R' => 44,
    'S' => 45, 'T' => 46, 'U' => 47, 'V' => 48,
    'W' => 49, 'X' => 50, 'Y' => 51, 'Z' => 52
);

$x = 1;
$sum = 0;
$same = array();
if ($file) {
    while (($line = fgets($file)) !== false) {
        if ($x > 3) {
            $x = 1;
            $same = array();
        }
        $part[$x] = str_split($line);

        if ($x === 3) {
            foreach ($part[1] as $key => $value) {
                if (in_array($value, $part[2], true)) {
                    $same[] = $value;
                }
            }
            foreach ($same as $key => $value) {
                if (in_array($value, $part[3], true)) {
                    $sum += $alphabet[$value];
                    break;
                }
            }
        }
        $x++;
    }
    fclose($file);
} else {
    echo "Error opening file";
}

echo $sum;