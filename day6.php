<?php
$filename = "input";
$file = fopen($filename, 'rb');


if ($file) {
    while (($line = fgets($file)) !== false) {
        $line = str_split(trim($line));
        $buffer = array();
        for ($i = 0, $iMax = count($line); $i < $iMax; $i++) {
            $buffer[] = $line[$i];
            $unique = array_unique($buffer);
            if (count($unique) === 14) {
                $i++;
                break;
            }

            if (count($buffer) === 14) {
                array_shift($buffer);
            }

        }
        echo $i . "\n";
    }
    fclose($file);
} else {
    echo "Error opening file";
}
