<?php
$filename = "input";
$file = fopen($filename, 'rb');

$grid = array_fill(0, 14, array_fill(0, 26, '.'));
$trace1[] = $posH = $posT = [8, 11];
$trace2[] = array();
$pos2 = array_fill(0, 9, [8, 11]);
$y = $posT[0];
$x = $posT[1];
if ($file) {
    while (($line = fgets($file)) !== false) {
//        echo $line;
        $line = explode(' ', trim($line));

        for ($i = 0; $i < (int)$line[1]; $i++) {
            switch ($line[0]) {
                case 'R':
                    $x++;
                    break;
                case 'L':
                    $x--;
                    break;
                case 'U':
                    $y--;
                    break;
                case 'D':
                    $y++;
                    break;
            }
            $lastPosH = $posH;
            $posH = [$y, $x];
            switch ($posH) {
                case [$posT[0], $posT[1]]:
                case [$posT[0], $posT[1] + 1]:
                case [$posT[0], $posT[1] - 1]:
                case [$posT[0] + 1, $posT[1]]:
                case [$posT[0] - 1, $posT[1]]:
                case [$posT[0] + 1, $posT[1] + 1]:
                case [$posT[0] + 1, $posT[1] - 1]:
                case [$posT[0] - 1, $posT[1] + 1]:
                case [$posT[0] - 1, $posT[1] - 1]:
                    break;
                default:
                    $pos2[0] = $posT = $lastPosH;
                    break;
            }
            $trace[] = $posT;
            for($j = 1; $j < 9; $j++) {
                switch ($pos2[$j - 1]) {
                    case [$pos2[$j][0], $pos2[$j][1]]:
                    case [$pos2[$j][0], $pos2[$j][1] + 1]:
                    case [$pos2[$j][0], $pos2[$j][1] - 1]:
                    case [$pos2[$j][0] + 1, $pos2[$j][1]]:
                    case [$pos2[$j][0] - 1, $pos2[$j][1]]:
                    case [$pos2[$j][0] + 1, $pos2[$j][1] + 1]:
                    case [$pos2[$j][0] + 1, $pos2[$j][1] - 1]:
                    case [$pos2[$j][0] - 1, $pos2[$j][1] + 1]:
                    case [$pos2[$j][0] - 1, $pos2[$j][1] - 1]:
                        break;
                    default:
                        $y2 = $pos2[$j - 1][0] < $pos2[$j][0] ? $pos2[$j][0] - 1 : $pos2[$j][0];
                        $y2 = $pos2[$j - 1][0] > $pos2[$j][0] ? $pos2[$j][0] + 1 : $y2;
                        $x2 = $pos2[$j - 1][1] < $pos2[$j][1] ? $pos2[$j][1] - 1 : $pos2[$j][1];
                        $x2 = $pos2[$j - 1][1] > $pos2[$j][1] ? $pos2[$j][1] + 1 : $x2;

                        $pos2[$j] = [$y2, $x2];
                        break;
                }
            }
            $grid[$pos2[8][0]][$pos2[8][1]] = '#';
//            echo $pos2[0][0] . ' ' . $pos2[0][1] . PHP_EOL;
//            echo $pos2[1][0] . ' ' . $pos2[1][1] . PHP_EOL;
            $trace2[] = $pos2[8];
        }
    }
    fclose($file);
} else {
    echo "Error opening file";
}

echo count(array_unique($trace, SORT_REGULAR)) . PHP_EOL;
echo count(array_unique($trace2, SORT_REGULAR)) . PHP_EOL;
//foreach ($grid as $row) {
//    echo implode('', $row) . PHP_EOL;
//}