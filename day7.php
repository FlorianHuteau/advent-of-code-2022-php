<?php
$filename = "input";
$file = fopen($filename, 'rb');

$tree = [
    '/' => [
        'weight' => 0,
        'parent' => ''
    ]
];
$parent = '';
if ($file) {
    while (($line = fgets($file)) !== false) {
        $line = explode(' ', $line);
        if($line[0] === '$'){
            if ($line[1] === 'cd') {
                if(trim($line[2]) === '..'){
                    $parent = $tree[$parent]['parent'];
                } else {
                    $parent .= trim($line[2]) !== '/' ? '/' . trim($line[2]): '/';
                }
            }
        } elseif ($line[0] === 'dir') {
            $tree[$parent !== '' ? $parent.'/'. trim($line[1]) : trim($line[1])] = [
                'weight' => 0,
                'parent' => $parent === '' ? '/' : $parent,
                'type' => 'dir'
            ];
        } else {
            $tree[trim($line[1])] = [
                'weight' => (int)$line[0],
                'parent' => $parent,
                'type' => 'file'
            ];
            $parentForWeight = $parent;
            while ($parentForWeight !== '')  {
                $tree[$parentForWeight]['weight'] += (int)$line[0];
                $parentForWeight = $tree[$parentForWeight]['parent'];
            }
        }
    }
    fclose($file);
} else {
    echo "Error opening file";
}

echo $tree['/']['weight'] . "\n";

$sizeMin = 30000000 - (70000000 - $tree['/']['weight']);
$dirSize = 30000000;
foreach ($tree as $key => $value) {
    if($value['type'] === 'dir' && $value['weight'] > $sizeMin){
        $dirSize > $value['weight'] ? $dirSize = $value['weight'] : null;
    }
}
echo $dirSize;